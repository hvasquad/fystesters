package com.mycompany.pdfprinttest;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLController implements Initializable {

    @FXML
    private TextField name;

    @FXML
    private TextField number;

    @FXML
    private TextField color;

    @FXML
    private TextField size;

    @FXML
    private TextField stickers;

    @FXML
    private TextField fourtyTwo;

    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        System.out.println("Export button clicked");
        PDFPrint pdfPrint = new PDFPrint(name, number, color, size, stickers, fourtyTwo);
        pdfPrint.print();
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {}
}
