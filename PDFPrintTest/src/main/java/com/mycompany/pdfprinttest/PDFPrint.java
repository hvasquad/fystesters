/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdfprinttest;

import java.awt.Color;
import java.io.IOException;
import javafx.scene.control.TextField;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 *
 * @author Daan Oolbekkink
 */
public class PDFPrint {
    
    private String textName;
    private String textNumber;
    private String textColor;
    private String textSize;
    private String textStickers;
    private String textFourtyTwo;

    public PDFPrint() {
    }
    
    public PDFPrint(TextField name, TextField number, TextField color, TextField size, TextField stickers, TextField fourtyTwo) throws IOException {
        this.textName = name.getText();
        this.textNumber = number.getText();
        this.textColor = color.getText();
        this.textSize = size.getText();
        this.textStickers = stickers.getText();
        this.textFourtyTwo = fourtyTwo.getText();
        
        
    }
    
    public void print() throws IOException{
        //PDF A4 Dimensions 595x842
        //create document
        PDDocument document = new PDDocument();
        //create page
        PDPage page1 = new PDPage();
        //add page to document
        document.addPage(page1);
        //create image
        PDImageXObject pdImage = PDImageXObject.createFromFile("C:/Users/Daan Oolbekkink/Documents/NetBeansProjects/FastenYourSeatbelt/src/main/resources/img/CorendonLogo.png", document);
        //create contentstream for page1
        int imageWidth = 112;
        int imageHeight = 75;
        PDPageContentStream contentStream = new PDPageContentStream(document, page1);
        contentStream.setNonStrokingColor(204, 0, 0);
        contentStream.addRect(0, 690, 620, 5);
        contentStream.fill();
        contentStream.setNonStrokingColor(Color.BLACK);
        contentStream.drawImage(pdImage, 5, 706, imageWidth, imageHeight);
        
        contentStream.beginText();
        contentStream.setFont(PDType1Font.COURIER, 24);
        contentStream.newLineAtOffset(180, 730);
        contentStream.setLeading(14.5f);
        contentStream.showText("Corendon Lost-and-Found");
        contentStream.endText();
        
        contentStream.beginText();
        contentStream.newLineAtOffset(220, 600);
        contentStream.setLeading(14.5f);
        contentStream.setFont(PDType1Font.COURIER, 12);
        contentStream.showText("Name:");
        contentStream.newLine();
        contentStream.showText("Number:");
        contentStream.newLine();
        contentStream.showText("Color:");
        contentStream.newLine();
        contentStream.showText("Size:");
        contentStream.newLine();
        contentStream.showText("Stickers:");
        contentStream.newLine();
        contentStream.showText("42?");
        contentStream.endText();
        
        contentStream.beginText();
        contentStream.newLineAtOffset(300, 600);
        contentStream.setLeading(14.5f);
        contentStream.setFont(PDType1Font.COURIER, 12);     
        contentStream.showText(this.textName);
        contentStream.newLine();
        contentStream.showText(this.textNumber);
        contentStream.newLine();
        contentStream.showText(this.textColor);
        contentStream.newLine();
        contentStream.showText(this.textSize);
        contentStream.newLine();
        contentStream.showText(this.textStickers);
        contentStream.newLine();
        contentStream.showText(this.textFourtyTwo);
        contentStream.endText();
        contentStream.close();
        //save document
        document.save("C:/Users/Daan Oolbekkink/Desktop/PDFTest.pdf");
        //close document
        document.close();
    }
}
